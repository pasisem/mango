import axios from "axios";
import { MangaEden } from "@/namespaces.ts";

class MangaEdenService {
  urlList: string = "https://www.mangaeden.com/api/list/0/";
  urlMangaDetails: string = "https://www.mangaeden.com/api/manga/";
  urlMangaChapter: string = "https://www.mangaeden.com/api/chapter/";
  page: number = 0;
  limit: number = 25;

  constructor() {
    console.log("Constructing a Manga Eden service");
  }
  // unused
  // getMangaListLimited(page?: number) {
  //   if (page) this.page = page;
  //   console.log("Requesting MangaEden list");
  //   return axios.get<MangaEden.MangaList>(
  //     this.url + "?p=" + this.page + "&l=" + this.limit
  //   );
  // }
  getMangaList() {
    console.log("Requesting MangaEden list");
    return axios.get<MangaEden.MangaList>(this.urlList);
  }
  getMangaDetails(id: string) {
    console.log("Requesting MangaEden details for -" + id + "-");
    // return axios.get<MangaEden.MangaDetails>(this.getMangaDetails + id + "/");
    return axios.get<any>(this.urlMangaDetails + id + "/");
  }
  getMangaChapter(id: string) {
    console.log("Requesting MangaEden chapter for -" + id + "-");
    // return axios.get<MangaEden.MangaDetails>(this.getMangaDetails + id + "/");
    return axios.get<any>(this.urlMangaChapter + id + "/");
  }
}

export const mangaEdenService = new MangaEdenService();
