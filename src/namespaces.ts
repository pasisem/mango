export namespace MangaEden {
  export const imgUrlPrefix: string = "https://cdn.mangaeden.com/mangasimg/";

  export interface MangaList {
    end?: number;
    manga?: ListItem[];
    page?: number;
    start?: number;
    total?: number;
  }
  export interface ListItem {
    a?: string;
    c?: string[];
    h?: number;
    i?: string;
    im?: string;
    s?: number;
    t?: string;
    ld?: string;
  }

  export interface MangaDetails {
    aka: string[];
    "aka-alias": string[];
    alias: string;
    artist: string;
    artist_kw: string[];
    author: string;
    author_kw: string[];
    autoManga: boolean;
    baka: boolean;
    categories: string[];
    chapters: Chapter[];
    chapters_len: number;
    created: string;
    description: string;
    hits: number;
    image: string;
    imageURL: string;
    language: number;
    last_chapter_date: string;
    released: number;
    startsWith: string;
    status: 2;
    title: string;
    title_kw: string[];
    type: number;
    updatedKeywords: boolean;
    url: string;
  }

  export interface Chapter {
    num: string;
    date: string;
    title: string;
    id: string;
  }

  export interface Image {
    num: number;
    url: string;
    width: number;
    height: number;
  }
}
